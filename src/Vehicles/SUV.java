/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicles;

/**
 *
 * @author Larvitar
 */
public class SUV extends Vehicle{
    String make;
    String model;
    int year;
    int doors;
    public SUV(String vinId, String make, String model, int year, int doors)
    {
        super(vinId);
        this.make = make;
        this.model = model;
        this.year = year;
        this.doors = doors;
    }
    public String toString()
    {
        return doors + " door " + make + " " + model;
    }
    public String getMake()
    {
        return make;
    }
    public String getModel()
    {
        return model;
    }
    public int getYear()
    {
        return year;
    }
    public int getDoors()
    {
        return doors;
    }
    
}
