/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.sql.*;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    
import java.util.UUID;
import javax.swing.*;


import users.*;
import Vehicles.*;
//class with the mysql server login details
import main.DB;

/**
 *
 * @author Larvitar
 */
public class DBInterface {
    private Connection connect;
    private PreparedStatement pStatement;
    private ResultSet results;
    private static JFrame f;
    private DateTimeFormatter dtf;
    LocalDateTime now;
    String today;
    //going to remove this and have the database connect via a library later
    public DBInterface()
    {   
        try{
        connect = DB.connect("vehiclerentalprogram");
        }
        catch(Exception e)
        {
            System.err.println("Error loading database\n" + e);
        }
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
        now = LocalDateTime.now();  
        today = dtf.format(now);
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      MANAGER                   ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    void viewManaged(Employee employee) {
        /**
         * got it working after all the changes, right now just need to update how
         * it shows up in the jpanel in order for it to be more readable, will look
         * into that later
         */
        String queryString = "";
        String titleString = "";
        String managerName = employee.getUsername();
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.employees WHERE manager = '"+
                    managerName+"'");
            results = pStatement.executeQuery(); 
            titleString = "username location date hired";
            while (results.next()) 
            {
                queryString = queryString.concat(results.getString("userName") + results.getString("location") + results.getString("date hired"));
                //System.out.println(queryString);
                queryString = queryString.concat("\n");
            }
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error viewing managed\n" + e);
        }
        titleString = titleString.concat("\n" + queryString);
        JOptionPane.showMessageDialog(f,titleString);
    }
    void viewCustomers() {
        //going to make another method for a manager/owner to see all customers from all locations
        String queryString = String.format("%-30s","User name") + String.format("%-30s","Renting") + String.format("%-30s","Location")+String.format("%-25s","vehicle rented") +"\n";
        String custInfo;
        String renting;
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer");
            results = pStatement.executeQuery();        
            /**
             * Some minor things wrong with this implementation but it works
             * there's a comma that shows up at the end of the line, would need to remove
             * consider adding the output into a box format of some sort
             */
            while (results.next()) 
            {
                //renting is a separate variable for readability sake, do not want a line to go on forever 
                renting = String.format("%-30s",results.getString("Renting").equals("0") ? "Not renting": "Renting");
                custInfo = String.format("%-30s",results.getString("userName")) + renting + String.format("%-30s",results.getString("Location")) + String.format("%-25s",results.getString("vehicleRented"));
                queryString = queryString.concat(custInfo + "\n");
            }
            JOptionPane.showMessageDialog(f,queryString);
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"SQL Syntax error\n" + e);
        }
    }
    
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      USER                      ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    
    /**
     * @param user
     * @return 
     * method used to determine if a username is already taken, checks the database 
     * to make sure the current username isn't already in the database
     */
    public boolean validateUser(String user) {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.users WHERE userName = '"+
                    user+"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                /**
                 * if the result from the query > 0 return true, shouldn't need to
                 * bother to check it as the query should return all values with
                 * the specified username
                 */
                return false;
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error validating user\n" + e);
        }
        return true;
    }
    /**
     * 
     * @param user 
     * basic method to delete user from database
     */
    public void deleteUser(String user)
    {
        try
        {
            pStatement = connect.prepareStatement("DELETE FROM `vehiclerentalprogram`.`users` WHERE userName = \""+
                    user+"\"");
            pStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error deleting user\n" + e);
        }
    }
    public User getUser(String user, String password)
    {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.users WHERE userName = '"+
                        user+"' AND Password = '" + password+"'");
            results = pStatement.executeQuery();
            /**
             * if results has a next add everything that's
             */
            if (results.next()) 
            {
                return new User(results.getString("userName"),results.getString("Password"), results.getString("userType"));
            }
            
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error getting user\n" + e);
        }
        return null;
    } 
    public boolean findUser(String username)
    {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.users WHERE userName = '"+
                    username+"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                /**
                 * if the result from the query > 0 return true, shouldn't need to
                 * bother to check it as the query should return all values with
                 * the specified username and the database is set up such that 
                 * the username is a unique value
                 */
                return true;
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error finding user"+e);
        }
        JOptionPane.showMessageDialog(f,"User not found in database");
        return false;
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      EMPLOYEE                  ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public boolean addEmployee(Employee add)
    {
        try
        {
            // will eventually update so that this becomes a singular composite query
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`users` (`userName`, `Password`,`id`, `userType`) VALUES ('"
                    + add.getUsername() +"', '" + add.getPassword() + "','"+UUID.randomUUID()+"','"+add.getUserType()+"');");
            JOptionPane.showMessageDialog(f,pStatement);
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`employees` (`userName`, `location`, `date hired`, `manager`) VALUES ('"+
                    add.getUsername() + "', '"+ add.getLocation() + "', '" + add.getDateHired() + "', '" + add.getManager() + "');");
            pStatement.executeUpdate();
            if(add.isManager())
            {
                //if the employee to be added is a manager, add to the manager database
                pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`manager` (`userId`) VALUES ('"
                    + add.getUsername()+"');");
                pStatement.executeUpdate();
            }
            return true;
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding employee\n"+e);
        }
        return false;
    }
    public void deleteEmployee (String user)
    {
        boolean found = findUser(user);
        if(!found)
        {
            JOptionPane.showMessageDialog(f,user+" not found in database, could not delete");
            return;
        }
        try
        {
            pStatement = connect.prepareStatement("DELETE FROM `vehiclerentalprogram`.`employees` WHERE userName = \""+
                    user+"\"");
            pStatement.executeUpdate();
            deleteUser(user);
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error deleting user\n"+e);
        }
        JOptionPane.showMessageDialog(f,"Deleted user: "+user);
    }
    public User getEmployee(User user, boolean isManager) {
        boolean manager = false;
        int col;
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.employees WHERE userName = '"+
                        user.getUsername()+"'");
            results = pStatement.executeQuery();
            if (results.next()) 
            {
                String type = isManager ? "manager" : "employee";
                return  new Employee(user.getUsername(), user.getPassword(), results.getString("location"), results.getString("manager"), results.getString("date hired"), type);
            }
            
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error getting employee\n"+e);
        }
        return null;
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      CUSTOMER                  ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    void viewCustomers(Employee employee) {
        //going to make another method for a manager/owner to see all customers from all locations
        String queryString = String.format("%-30s","User name") + String.format("%-30s","Renting") + String.format("%-30s","Location")+String.format("%-25s","vehicle rented") +"\n";
        String custInfo;
        String renting;
        String location = employee.getLocation();
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer WHERE location = '"+
                    location+"'");
            results = pStatement.executeQuery();        
            /**
             * Some minor things wrong with this implementation but it works
             * there's a comma that shows up at the end of the line, would need to remove
             * consider adding the output into a box format of some sort
             */
            while (results.next()) 
            {
                //renting is a separate variable for readability sake, do not want a line to go on forever 
                renting = String.format("%-30s",results.getString("Renting").equals("0") ? "Not renting": "Renting");
                custInfo = String.format("%-30s",results.getString("userName")) + renting + String.format("%-30s",results.getString("Location")) + String.format("%-25s",results.getString("vehicleRented"));
                queryString = queryString.concat(custInfo + "\n");
            }
            JOptionPane.showMessageDialog(f,queryString);
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"SQL Syntax error\n" + e);
        }
    }
    public boolean addCustomer(Customer add) {
        try
        {
            // will eventually update so that this becomes a singular composite query
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`users` (`userName`, `Password`,`id`, `userType`) VALUES ('"
                    + add.getUsername() +"', '" + add.getPassword() + "','"+UUID.randomUUID()+"','"+add.getUserType()+"');");
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`customer` (`userName`, `Renting`, `Location`) VALUES ('"+
                    add.getUsername() + "', '"+ add.isRenting() + "', '" + add.getLocation() + "');");
            pStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error viewing customer\n" + e);
        }
        return false;
    }
    public boolean deleteCustomer(String user)
    {
        boolean found = findUser(user);
        if(found)
        {
            deleteCustomerAction(user);
            return true;
        }
        JOptionPane.showMessageDialog(f,user+" not found in database, could not delete");
        return false;
    }
    public void deleteCustomerAction(String user)
    {
        try
        {
            pStatement = connect.prepareStatement("DELETE FROM `vehiclerentalprogram`.`customer` WHERE userName = \""+
                    user+"\"");
            pStatement.executeUpdate();
            deleteUser(user);
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error deleting customer\n" + e);
        }
        JOptionPane.showMessageDialog(f,"Deleted user: "+user);
    }
    public Customer getCustomer(User user) {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer WHERE userName = '"+
                        user.getUsername()+"'");
            results = pStatement.executeQuery();
            while (results.next()) 
            {
                boolean renting = !results.getString("Renting").equals(0);
                return new Customer(user.getUsername(), results.getString("Location"), renting, user.getPassword(), results.getString("companyWorkedFor"),results.getString("vehicleRented"), results.getString("lastVehicleRented"));
                //(String username, String location, boolean rent, String password, String companyWorkedFor
            }
            
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error getting customer\n" + e);
        }
        return null;
    }
    public boolean findCustomer(String username)
    {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer WHERE userName = '"+
                    username+"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                /**
                 * if the result from the query > 0 return true, shouldn't need to
                 * bother to check it as the query should return all values with
                 * the specified username
                 */
                return true;
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error finding customer\n" + e);
        }
        JOptionPane.showMessageDialog(f,"Customer not found in database");
        return false;
    }
    public void showCustomer(String username){
        String titleString = String.format("%-25s", "Username")+String.format("%-25s", "Renting")+String.format("%-25s", "Location")+String.format("%-25s", "Company Worked For")+String.format("%-25s","Vehicle Rented");
        String queryString="";
        try{
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer WHERE username = '" + username +"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                String company = results.getString("companyWorkedFor") == null ? String.format("%-25s","") : String.format("%-25s",results.getString("companyWorkedFor"));
                String renting = results.getString("Renting").equals("1")? "Yes":"No";
                queryString = String.format("%-25s",results.getString("userName"))+
                        String.format("%-25s",renting) + String.format("%-25s",results.getString("Location"))+
                        company + String.format("%-25s",results.getString("vehicleRented"));
            }
            JOptionPane.showMessageDialog(f,titleString+"\n" + queryString);
            System.out.println(titleString+"\n"+queryString);
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error showing customer\n" + e);
        }
    }
    
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      Vehicle                   ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public boolean addCar(Car car)
    {
        try
        {
            // will eventually update so that this becomes a singular composite query
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`vehicles` "
                    + "(`VINNumber`, `RentedBy`, `Make`, `Model`, `Year`, `Doors`, `Type`) "
                    + "VALUES ('"+car.getVinId() +"', null"+", '" + car.getMake() + "', '" + car.getModel() + "', " + car.getYear() + ", " + car.getDoors() + ", 'Car');");
            System.out.println(pStatement);
            pStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding car\n"+e);
        }
        return false;
    }
    public boolean addSUV(SUV suv)
    {
        try
        {
            // will eventually update so that this becomes a singular composite query
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`vehicles` "
                    + "(`VINNumber`, `RentedBy`, `Make`, `Model`, `Year`, `Doors`, `Type`) "
                    + "VALUES ('"+suv.getVinId() +"', null"+", '" + suv.getMake() + "', '" + suv.getModel() + "', " + suv.getYear() + ", " + suv.getDoors() + ", 'SUV');");
            pStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding SUV\n"+e);
        }
        return false;
    }
    public boolean addTruck(Truck truck)
    {
        try
        {
            // will eventually update so that this becomes a singular composite query
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`vehicles` "
                    + "(`VINNumber`, `RentedBy`, `Make`, `Model`, `Year`, `Doors`, `Type`) "
                    + "VALUES ('"+truck.getVinId() +"', null"+", '" + truck.getMake() + "', '" + truck.getModel() + "', " + truck.getYear() + ", " + truck.getDoors() + ", 'Truck');");
            pStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding Truck\n"+e);
        }
        return false;
    }
    public void viewAllVehicles()
    {
        String titleString = String.format("%-25s","VIN Number") + String.format("%-25s", "Rented by") + 
                String.format("%-25s", "Make") + String.format("%-25s","Model") + String.format("%-25s", "Year") + 
                String.format("%-25s","Number of doors") + String.format("%-25s","Type");
        String queryString = "";
        String vehicleInfo;
        try
        {
            /**
             * again the j option pane formatting is getting in the way
             * for some reason it isn't formatting correctly might fix later, might leave it alone
             */
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.vehicles;");
            results = pStatement.executeQuery();
            while (results.next()) 
            {
                vehicleInfo = String.format("%-25s",results.getString("VINNumber"))+String.format("%-25s",results.getString("Rentedby"))+
                        String.format("%-25s",results.getString("Make"))+String.format("%-25s",results.getString("Model"))+
                        String.format("%-25s",results.getString("Year")) + String.format("%-25s",results.getString("Doors"))+
                        String.format("%-25s",results.getString("Type"));
                queryString = queryString.concat(vehicleInfo+"\n");
            }
            if(queryString.equals(""))
                queryString = "No vehicles found in database.";
            else
                queryString = titleString.concat("\n"+queryString);
             JOptionPane.showMessageDialog(f,queryString);
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error viewing all vehicles\n"+e);
        }
    }
    public void deleteVehicle(String vinid)
    {
        try
        {
            pStatement = connect.prepareStatement("DELETE FROM `vehiclerentalprogram`.`vehicles` WHERE VINNumber = \""+
                    vinid+"\"");
            pStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error deleting vehicle \n"+vinid);
        }
        JOptionPane.showMessageDialog(f,"Deleted vehicle: "+vinid);
    }
    public void rentVehicle(String vin, String user)
    {
        if(!isVehicleRented(vin))
        {
           JOptionPane.showMessageDialog(f,"Vehicle is already rented");
           return;
        }
        try
        {
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`customer`\n" +
                                                    "SET\n" +
                                                    "`Renting` = 1,\n"+
                                                    "`vehicleRented` = '"+vin+"',\n"+
                                                    "`dateRented` = '"+today+"'\n" +
                                                    "WHERE `userName` = '"+user+"';");
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`vehicles`\n" +
                                                    "SET\n" +
                                                    "`RentedBy` = \""+user+"\"\n" +
                                                    "WHERE `VINNumber` = '"+vin+"';");
            pStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error renting vehicle\n" + e);
        }
    }
    public boolean findVehicle(String vinid)
    {
        try
        {
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.vehicles WHERE VINNumber = '"+
                    vinid+"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                /**
                 * if the result from the query > 0 return true, shouldn't need to
                 * bother to check it as the query should return all values with
                 * the specified username
                 */
                return true;
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(f,"Error finding vehicle\n"+e);
        }
        f = new JFrame();
        JOptionPane.showMessageDialog(f,"Vehicle not found in database");
        return false;
    }
    public void returnVehicle(String renter){
        try
        {
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`customer`\n" +
                                                    "SET" +
                                                    "`Renting` = 0,\n" +
                                                    "`vehicleRented` = NULL,\n" +
                                                    "`dateRented` = NULL\n" +
                                                    "WHERE `userName` = '"+renter+"';");
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`vehicles`\n" +
                                                    "SET\n" +
                                                    "`RentedBy` = NULL\n" +
                                                    "WHERE `RentedBy` = \""+renter+"\";");
            pStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error returning vehicle\n" + e);
        }
    }
    public boolean isVehicleRented(String vin)
    {
        try{
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.vehicles WHERE VINNumber = '"+
                    vin+"'");
            results = pStatement.executeQuery();
            if(results.next())
            {
                return results.getString("RentedBy") == null;
            }
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error checking vehicle rental status\n"+e);
        }
        return false;
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      Account                   ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//'
    public void addAccount(String company)
    {
        try
        {
            pStatement = connect.prepareStatement("INSERT INTO `vehiclerentalprogram`.`accounts`"
                    + "(`companyName`, `numberOfEmployees`)"
                    + "VALUES"
                    + "('"+ company + "', 0);");
            pStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding account\n"+e);
        }
    }
    public void deleteAccount(String companyName)
    {
        try
        {
            pStatement = connect.prepareStatement("DELETE FROM `vehiclerentalprogram`.`accounts` WHERE companyName = \""+
                    companyName+"\"");
            pStatement.executeUpdate();
            
        JOptionPane.showMessageDialog(f,"Deleted Account: "+companyName);
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error deleting account\n"+e);
        }
    }
    public void viewAllAccounts()
    {
        String titleString = String.format("%-25s", "Company Name") + String.format("%-25s", "Number of employees");
        String queryString = "";
        try
        {
            /**
             * again the j option pane formatting is getting in the way
             * for some reason it isn't formatting correctly might fix later, might leave it alone
             */
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.accounts;");
            results = pStatement.executeQuery();
            while (results.next()) 
            {
                queryString = queryString.concat(String.format("%-35s",results.getString("companyName")) + String.format("%-25s",results.getString("numberOfEmployees")+"\n"));
            }
            if(queryString.equals(""))
                queryString = "No accounts found in database.";
            else
                queryString = titleString.concat("\n"+queryString);
            JOptionPane.showMessageDialog(f,queryString);
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error viewing accounts\n" + e);
        }
    }
    public void addUserToAccount(String user, String account)
    {
        try
        {
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`customer`\n" +
            "SET\n" +
            "`companyWorkedFor` = \""+account+"\"\n" +
            "WHERE `userName` = \""+user+"\";");
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`accounts`\n" +
            "SET\n" +
            "`numberOfEmployees` = numberOfEmployees + 1\n" +
            "WHERE `companyName` = \""+account+"\";");
            pStatement.executeUpdate();
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error adding user to account\n"+e);
        }
    }
    public void removeUserFromAccount(String user, String account)
    {
        try{
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`customer`\n" +
            "SET\n" +
            "`companyWorkedFor` = null\n" +
            "WHERE `userName` = \""+user+"\";");
            pStatement.executeUpdate();
            pStatement = connect.prepareStatement("UPDATE `vehiclerentalprogram`.`accounts`\n" +
            "SET\n" +
            "`numberOfEmployees` = numberOfEmployees - 1\n" +
            "WHERE `companyName` = \""+account+"\";");
            pStatement.executeUpdate();
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error removing user to account\n"+e);
        }
    }
    public void showAllUsersOnAccount(String user)
    {
        String titleString = String.format("%-25s", "Name") + String.format("%-25s", "Renting") + 
                String.format("%-25s", "Location") + String.format("%-25s", "Company Worked for");
        String queryString = "";
        try
        {
            /**
             * again the j option pane formatting is getting in the way
             * for some reason it isn't formatting correctly might fix later, might leave it alone
             */
            pStatement = connect.prepareStatement("SELECT * FROM vehiclerentalprogram.customer\n"
                    + "WHERE\n"
                    + "companyWorkedFor = \""+user+"\";");
            results = pStatement.executeQuery();
            while (results.next()) 
            {
                String renting = results.getString("Renting").equals("1")?"Yes":"No";
                String h = String.format("%-25s",results.getString("userName")) + 
                        String.format("%-25s",renting) + 
                        String.format("%-25s",results.getString("Location")) +
                        String.format("%-25s",results.getString("companyWorkedFor"));
                queryString = queryString.concat(h+"\n");
            }
            if(queryString.equals(""))
                queryString = "No accounts found in database.";
            else
                queryString = titleString.concat("\n"+queryString);
            JOptionPane.showMessageDialog(f,queryString);
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(f,"Error showing all users on account\n"+e);
        }
    }
    /**
     * add methods to:
     * manager method to see all users in database
     */
}