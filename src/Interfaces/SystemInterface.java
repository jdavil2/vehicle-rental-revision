/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import GUIForms.*;
import Vehicles.*;
import Cost.*;
import java.util.ArrayList;
import javax.swing.*;
import users.*;
/**
 *
 * @author Larvitar
 */
public class SystemInterface {
    JFrame f = new JFrame();
    Vehicle vehicle;
    ArrayList listOfVehicles = new ArrayList<Vehicle>();
    Customer customer;
    User user;
    DBInterface db;
    Accounts account;
    Rates rates = new Rates();
    Rates corporateRates = new CorporateRates();
    emailInterface emailInterface;
    //String user;
    /**
     * System interface is where all the separate classes get tied together, some 
     * of the calculations go through here, all the GUI stuff connects to each 
     * other through here, the separate interfaces connect to each other here as well
     * Some basic user information will be stored here as well, in the current 
     * implementation
     */
    private static SystemInterface instance = new SystemInterface();
    
    private SystemInterface(){}
    
    public static SystemInterface getInstance()
    {
        return instance;
    }
    public void start()
    {
        //load the database and the start up GUI when program is started
        db = new DBInterface();
        emailInterface = new emailInterface();
        UserInterface.startGui();
    }
    
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      USER                      ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public void deleteEmployee() {
        /**
         * only a manager should be able to remove employees maybe
         * Should add some sort of input validation
         */
        String user = JOptionPane.showInputDialog(f,"Enter username");
        db.deleteEmployee(user);
    }
    public boolean validateUser(String user)
    {
        //validate and make sure that a username isn't taken
        return db.validateUser(user);
    }
    public User checkuser(String user, String password)
    {
        return db.getUser(user, password);
    }
    public String userType(String userName) {
        return user.getUserType();
    }
    public String getUsername()
    {
        return user.getUsername();
    }
    
    public boolean getUser(String username, String password)
    {
        user = db.getUser(username, password);
        return !(user == null);
    }
    /**
     * almost completely unnecessary, just using this to delete all references 
     * upon sign out, decreasing the likelihood that something stupid I do later 
     * on will cause some sort of error, basic idea is to remove all references 
     * to objects that were only needed in the previous instance used upon sign 
     * out
     * Since I am going to reset the system interface objects everytime I log out, 
     * I decided to bring it together with the start login page to create a single 
     * method. It does two things but until I see a need to do one of those things
     * without the other, they will remain being done in this method.
     */
    public void startUser()
    {
        vehicle = null;
        listOfVehicles = null;
        customer = null;
        user = null;
        account = null;
        UserInterface.startGui();
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      EMPLOYEE                  ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public void getEmployee(boolean isManager)
    {
        user = db.getEmployee(user, isManager);
    }
    public void addNewEmployee()
    {
        NewEmployee.startGui();
    }
    
    public boolean addNewEmployeeToDb(Employee add)
    {
        return(db.addEmployee(add));
    }
    public void startEmployee() {
        EmployeeInterface.startGui();
    }
    public void returnVehicle()
    {
        String renter = JOptionPane.showInputDialog(f,"Enter the customers name");
        db.returnVehicle(renter);
        //currently only being used as a check to see if everything is working accordingly  
        JOptionPane.showMessageDialog(f, "the amount to be paid is: "+amountToBePaid(5, "daily", "SUV"));
    }
    //******************************************************************//
    //**************                                      **************//
    //**************                                      **************//
    //**************            MANAGER                   **************//
    //**************            METHODS                   **************//
    //**************                                      **************//
    //**************  Employee and manager are very       **************//
    //**************  similar so manager only needs       **************//
    //**************  a few specific methods for the      **************//
    //**************  few things that a regular employee  **************//
    //**************  can't do                            **************//
    //******************************************************************//
    
    public void viewManaged() {
        db.viewManaged((Employee)user);
    }

    public void startManager()
    {
        ManagerInterface.startGui();
    }
    public void managerViewAllCustomers(){
        db.viewCustomers();
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      CUSTOMER                  ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public void viewCustomerInfo() {
        String user = JOptionPane.showInputDialog(f,"Enter username");
        db.showCustomer(user);
    }
    public void getCustomer()
    {
        customer = db.getCustomer(user);
    }
    public boolean addCustomerToDB(Customer add) {
        return db.addCustomer(add);
    }
    public void deleteCustomer() {
        //need to add input validation
        String user = JOptionPane.showInputDialog(f,"Enter username");
        db.deleteCustomerAction(user);
    }
    public void viewAllCustomers()
    {
        db.viewCustomers((Employee)user);
    }
    public void addNewCustomer() {
        NewCustomer.startGui();
    }
    public void startCustomer() {
        CustomerInterface.startGui();
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      Account                   ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public void addNewAccount()
    {
        String name = JOptionPane.showInputDialog(f,"Enter Company Name"); 
        db.addAccount(name);
    }
    public void deleteAccount()
    {
        String name = JOptionPane.showInputDialog(f,"Enter Company to be removed");
        db.deleteAccount(name);
    }
    public void viewAllAccounts()
    {
        db.viewAllAccounts();
    }
    /**
     * decided that only managers should be able to access corporate account details
     */
    public void addUserToAccount()
    {
        String user = JOptionPane.showInputDialog(f,"Enter user to add to account");
        String account = JOptionPane.showInputDialog(f,"Enter account");
        db.addUserToAccount(user,account);
    }
    public void removeFromAccount()
    {
        String user = JOptionPane.showInputDialog(f,"Enter user to remove to account");
        String account = JOptionPane.showInputDialog(f,"Enter account");
        db.removeUserFromAccount(user, account);
    }
    public void viewAllUsersOnAccount()
    {
        String account = JOptionPane.showInputDialog(f,"Enter the account name");
        db.showAllUsersOnAccount(account);
    }
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      Vehicle                   ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    
    public void addNewVehicle()
    {
        NewVehicle.startNewVehicle();
    }
    public boolean addCarToDb(Car car)
    {
        return db.addCar(car);
    }
    public boolean addSUVToDb(SUV suv)
    {
        return db.addSUV(suv);
    }
    public boolean addTruckToDb(Truck truck)
    {
        return db.addTruck(truck);
    }
    public void viewAllVehicles()
    {
        db.viewAllVehicles();
    }
    public void deleteVehicle()
    {
        //need to add input validation
        String vinid = JOptionPane.showInputDialog(f,"Enter VIN Id");
        db.deleteVehicle(vinid);
    }
    public void rentVehicle()
    {
        String vin = JOptionPane.showInputDialog(f,"Enter Vehicle to be rented");
        if(!db.findVehicle(vin))
            return;
        String user = JOptionPane.showInputDialog(f,"Enter user to rent vehicle");
        if(!db.findCustomer(user))
            return;
        db.rentVehicle(vin, user);
    }
    
    //************************************************************************//
    //********************                                ********************//
    //********************                                ********************//
    //********************      Rate                    ********************//
    //********************      METHODS                   ********************//
    //********************                                ********************//
    //************************************************************************//
    public void estimateRates(String type, String timeUnits, String time)
    {
        int timeInt = Integer.parseInt(time);
        int rate = 0;
        switch(type)
        {
            case "car":
                if(timeUnits.equals("days"))
                    rate = rates.getDailyCarRate();
                else
                    rate = rates.getWeeklyCarRate();
                break;
            case "suv":
                if(timeUnits.equals("days"))
                    rate = rates.getDailySUVRate();
                else
                    rate = rates.getWeeklySUVRate();
                break;
            case "truck":
                if(timeUnits.equals("days"))
                    rate = rates.getDailyTruckRate();
                else
                    rate = rates.getWeeklyTruckRate();
                break;
        }
        if(rate == 0)
        {
            JOptionPane.showMessageDialog(f, "Invalid vehicle type selected");
        }
        else
            JOptionPane.showMessageDialog(f,"Estimate price for: "+timeInt+" "+timeUnits+" is $"+timeInt * rate);
    }
    public void viewRates()
    {
        if(customer.getCompanyWorkedFor() == null)
            JOptionPane.showMessageDialog(f,rates);
        else
            JOptionPane.showMessageDialog(f,corporateRates);
    }
    public void estimateRates()
    {
        CostEstimate.startGui();
    }
    public String amountToBePaid(int time, String units, String vehicleType){
        double amount = 0;
        switch(vehicleType){
            case "Car":
                if(units.equals("daily"))
                    amount = time * rates.getDailyCarRate();
                else
                    amount = time * rates.getWeeklyCarRate();
                break;
            case "SUV":
                if(units.equals("daily"))
                    amount = time * rates.getDailySUVRate();
                else
                    amount = time * rates.getWeeklySUVRate();
                break;
            case "Truck":
                if(units.equals("daily"))
                    amount = time * rates.getDailyTruckRate();
                else
                    amount = time * rates.getWeeklyTruckRate();
                break;
        }
        return "$" + amount;
    }
    /**
     * make change such that the employee and manager have different versions of
     * the same method ie: employees should only have access to vehicles and customers
     * based in their locations, managers should have access to all customers 
     * and vehicle information as to direct customers to the correct locations and
     * to allow customers to rent and return vehicles from different locations
     * maybe make it such that customers are only tied to a location based on their
     * vehicles, allowing any employee to return a vehicle 
     */
}
