/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import Vehicles.Vehicle;

/**
 *
 * @author Larvitar
 */
public class Customer extends User{
    String location;
    int renting;
    Vehicle vehicle;
    String companyWorkedFor;
    String lastVehicleRented;
    
    public Customer(String username, String location, boolean rent, String password, String companyWorkedFor,String vehicleRented ,String lastVehicleRented)
    {
        //password to gui
        super(username,password, "customer");
        this.location = location;
        //value of renting being represented as binary to be compliant with mysql database
        if(rent)
            renting = 1;
        else
            renting = 0;
        this.companyWorkedFor = companyWorkedFor;
    }
    public String getLocation()
    {
        return location;
    }
    public int isRenting()
    {
        return renting;
    }
    public void rent(Vehicle vehicle)
    {
        this.vehicle = vehicle;
        renting = 1;
    }
    public String getCompanyWorkedFor()
    {
        return companyWorkedFor;
    }
    public String toString()
    {
        if(renting == 0)
            return "Username: " + this.userName + "\nIs currently renting";
        return "Username: " + this.userName + "\nLocation renting from: " + location + "\nIs currently not renting";
    }
    
}
