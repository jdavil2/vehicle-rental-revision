/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;


/**
 *
 * @author Larvitar
 */
public class Employee extends User{
    private String location;
    private String managedBy;
    private String dateHired;
    
    // default constructor with null values
    public Employee(){};
    public Employee(String username, String password, String location, String managedBy, String dateHired, String type)
    {
        /**
         * considering making a manager subclass so far that is not necessary as 
         * all I really need is a boolean stating whether an employee is a manager 
         * or not, should only need to check it once on manager login, if it is 
         * necessary I will make one later
         */
        super(username, password, type);
        this.location = location;
        this.managedBy = managedBy;
        this.dateHired = dateHired;
    }
    public String toString()
    {
        if(type.equals("manager"))
            return "Employee Name: " + userName + "\nManager of: " + location + "\nDateHired: " + dateHired;
        return "Employee name: "+ userName + "\nLocation: " + location + "\nDate Hired: " + dateHired + "\nManaged by: " +managedBy;
    }
    public String getLocation()
    {
        return location;
    }
    public String getManager()
    {
        return managedBy;
    }
    public String getDateHired()
    {
        return dateHired;
    }
    public boolean isManager()
    {
        return type.equals("manager");
    }
}
