/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import java.util.*;

/**
 *
 * @author Larvitar
 */
public class Accounts {
    private String companyName;
    private ArrayList<Customer> accounts = new ArrayList();
    private int num;
    public Accounts(String companyName)
    {
        this.companyName = companyName;
        num = 0;
    }
    public String getCompanyName()
    {
        return companyName;
    }
    public void addUser(Customer user)
    {
        accounts.add(user);
        num++;
    }
    public String toString()
    {
        return "Company name: " + companyName + "\nNumber of users on account" + num;
    }
    
}
