/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

/**
 *
 * @author Larvitar
 */
public class User {
    String userName;
    String password;
    String type;
    
    public User()
    {
        
    }
    public User(String userName, String password, String type)
    {
        this.userName = userName;
        this.password = password;
        this.type = type;
    }
    public User(User user)
    {
        user.password = password;
        user.userName = userName;
    }
    public String getUsername()
    {
        return userName;
    }
    public String getPassword()
    {
        return password;
    }
    
    public String toString(){
        return "User is: " + userName;
    }
    public String getUserType(){
        return type;
    }
}
