/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cost;

/**
 *
 * @author Larvitar
 */
public class Rates {
    int carRateDay;
    int carRateWeek;
    int suvRateDay;
    int suvRateWeek;
    int truckRateDay;
    int truckRateWeek;
    public Rates()
    {
        carRateDay = 12;
        carRateWeek = 56;
        suvRateDay = 14;
        suvRateWeek = 65;
        truckRateDay = 13;
        truckRateWeek = 60;
    }
    public int getDailyCarRate()
    {
        return carRateDay;
    }
    public int getWeeklyCarRate()
    {
        return carRateWeek;
    }
    public int getDailySUVRate()
    {
        return suvRateDay;
    }
    public int getWeeklySUVRate()
    {
        return suvRateWeek;
    }
    public int getDailyTruckRate()
    {
        return truckRateDay;
    }
    public int getWeeklyTruckRate()
    {
        return truckRateWeek;
    }
    public String toString()
    {
        return "Daily rate for cars: $" + carRateDay+"\nWeekly rate for cars: $" + carRateWeek+
                "\n\n\nDaily rate for SUV's: $"+suvRateDay+"\nWeekly rate for SUV's: $"+suvRateWeek+
                "\n\n\nDaily rate for trucks: $"+truckRateDay+"\nWeekly rate for trucks: $"+truckRateWeek;
    }
}
